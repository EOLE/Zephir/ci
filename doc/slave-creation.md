# Ajouter un noeud d'exécution à Jenkins

## Procédure

### Dans Jenkins

1. Se connecter à Jenkins
2. Dans votre page de profil, cliquer sur `Configurer`.
3. Dans la section `API Token`, cliquer sur `Add new Token` et définir un nom arbitraire.
4. Cliquer sur `Generate` puis noter la chaine alphanumérique affichée.

### Dans OpenNebula

1. Se connecter à l'interface Sunstone d'OpenNebula
2. Instancier une nouvelle machine virtuelle à partir du template `jenkins-slave`. Renseigner les variables obligatoires suivantes:
    - `URL du "master" Jenkins pour l'enrôlement` Renseigner l'URL racine de votre instance Jenkins (par exemple: http://195.221.237.17/).
    - `Label(s) à associer à l'exécuteur Jenkins (séparés par des espaces)` Renseigner le ou les labels à associer à cet exécuteur.
    - `Mot de passe de l'utilisateur Jenkins` Renseigner le jeton d'authentification précedemment généré/noté dans Jenkins.
    - `Nom de l'utilisateur Jenkins` Renseigner votre identifiant utilisateur Jenkins.

L'exécuteur devrait démarrer et vous devriez le voir apparaitre dans la liste des exécuteurs sur l'interface Jenkins principale, avec comme nom `slave-<ip_machine>`.

## Liens

- [Recettes de génération d'images disques](https://forge.cadoles.com/Cadoles/cadoles-vm)
- [Usage des labels dans le Jenkinsfile](https://jenkins.io/doc/book/pipeline/jenkinsfile#using-multiple-agents)