# Envoyer un courriel au développeur depuis le pipeline

Il est possible d'envoyer un courriel au développeur à l'origine de l'exécution du pipeline, par exemple lors que l'exécution de celui ci échoue.

## Snippet

```groovy
pipeline {

    // On définit une section de "post exécution" dans notre pipeline.
    post {
        // On indique qu'on veut exécuter les opérations suivantes
        // quelle que soit le résultat d'exécution du pipeline.
        // D'autres conditions sont possibles, voir la documentation de Jenkins
        // pour plus d'informations.
        always {

            // On utilise la directive "emailext" pour envoyer un courriel.
            // On peut utiliser différentes variables d'environnement à injecter
            // dans le sujet du courriel ainsi que son "corps" pour renseigner le développeur
            // sur l'état d'exécution du pipeline.
            emailext (
                subject: "[${currentBuild.currentResult}] - ${env.JOB_NAME}/${env.BUILD_NUMBER}",
                body: """
                ${env.RUN_DISPLAY_URL}
                """.stripIndent(),
                recipientProviders: [developers(), requestor()],
            )
        }
    }

}
```

## Ressources

- [Documentation Jenkins - Configuration d'actions "post-exécution"](https://jenkins.io/doc/book/pipeline/syntax/#post)
- [Wiki Jenkins - Plugin `emailext`](https://wiki.jenkins.io/display/JENKINS/Email-ext+plugin)