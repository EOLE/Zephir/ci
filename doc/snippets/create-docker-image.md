# Créer une image Docker depuis un pipeline

Il est possible de construire une image Docker à partir du pipeline. Pour ce faire, il faut exposer Docker à l'intérieur du conteneur d'exécution.

## Snippet

Afin de pouvoir communiquer avec le daemon Docker, il nous faut utiliser une image avec le client Docker intégré pour l'exécution de notre pipeline.

```Dockerfile
# Fichier ci/Dockerfile

# On utilise l'image officielle
FROM docker:stable

# Il est tout à fait possible d'installer d'autres paquets nécessaires à l'exécution des
# stages de test
```

```groovy
pipeline {

    agent {
        dockerfile {
            filename 'Dockerfile'
            dir 'ci'
            // Le conteneur doit être "privilégié" afin qu'il puisse communiquer
            // avec le daemon Docker
            args '--privileged -v /var/run/docker.sock:/var/run/docker.sock'
        }
    }

    // Définition des "triggers" (i.e. déclaration du déclenchement du pipeline via Gitlab)
    triggers {
        gitlab(
            triggerOnPush: true, 
            triggerOnMergeRequest: true, 
            branchFilterType: 'All',
            cancelPendingBuildsOnUpdate: false
        )
    }

    options {
      gitLabConnection('Gitlab MIM')
      gitlabBuilds(builds: [])
    }

    // Paramètres de construction
    // Permet de personnaliser le déroulement du pipeline lors d'un déclenchement manuel
    parameters {
        string(defaultValue: "develop", description: 'Branche "staging"', name: 'stagingBranch')
        string(defaultValue: "master", description: 'Branche "stable"', name: 'stableBranch')
    }

    // Définition de variables d'environnement
    environment {
        IMAGE_NAME = "my-service-name"                      // Nom du service/de image Docker à générer
        IMAGE_COMMIT_TAG =  env.GIT_COMMIT.substring(0,8)   // Référence "courte" du commit
    }

    stages {

        stage('Build image') {
            steps {
                // On utilise la fonction personnalisée (voir plus bas)
                // permettant de déclarer l'exécution d'un "stage" à Gitlab
                gitlabStage(STAGE_NAME) {
                    // On utilise la fonction personnalisée
                    // qui exposer les identfiants d'accès à l'espace DockerHub
                    // sous forme de variables d'environnement
                    withDockerHubCredentials {
                        sh """
                            # Construction de l'image
                            docker build -t '${DOCKER_USERNAME}/${IMAGE_NAME}' .

                            # Ajout du tag de commit
                            docker tag '${DOCKER_USERNAME}/${IMAGE_NAME}:latest' '${DOCKER_USERNAME}/${IMAGE_NAME}:${IMAGE_COMMIT_TAG}'
                        """
                    }
                }
            }
        }

        stage('Publish staging image') {
            when {
                // Ce stage ne s'exécute que lorsque le commit à été réalisé sur la branche
                // "staging". Voir les paramètres de construction plus haut.
                branch params.stagingBranch
            }
            steps {
                gitlabStage(STAGE_NAME) {
                    addTagAndPublishDockerImage(
                        env.IMAGE_COMMIT_TAG, 
                        "staging-latest"
                    )
                }
            }
        }

        stage('Publish stable image') {
            when {
                // Ce stage ne s'exécute que lorsque le commit à été réalisé sur la branche
                // "stable". Voir les paramètres de construction plus haut.
                branch params.stableBranch
            }
            steps {
                gitlabStage(STAGE_NAME) {
                    addTagAndPublishDockerImage(
                        env.IMAGE_COMMIT_TAG, 
                        "stable-latest"
                    )
                }
            }
        }

    }

}


// Cette fonction personnalisée permet d'exécuter une fonction
// anonyme (closure) tout en la déclarant comme "stage" dans Gitlab
// Si une étape échoue, le stage Gitlab sera automatiquement marqué
// comme échoué. 
def gitlabStage(String stageName, Closure fn) {
    gitlabBuilds(builds: [stageName]) {
        gitlabCommitStatus(stageName) {
            fn()
        }
    }
}

// Cette fonction personnalisée permet d'exécuter une fonction anonyme
// tout en lui exposant les identifiants de connexion à l'espace DockerHub
// sous forme de variables d'environnement
def withDockerHubCredentials(Closure fn) {
    withCredentials([
        usernamePassword(credentialsId: 'dockerhub', usernameVariable: 'DOCKER_USERNAME', passwordVariable: 'DOCKER_PASSWORD')]
    ){
        fn()
    }
}

// Cette fonction personnalisée permet d'ajouter un tag à une image
// Docker présente dans le registre local du service Docker et de
// publier l'image avec ses deux tags.
def addTagAndPublishDockerImage(String originalTag, String newTag) {
    withDockerHubCredentials {
        sh """
            # Ajout du nouveau tag
            docker tag '${DOCKER_USERNAME}/${IMAGE_NAME}:${originalTag}' '${DOCKER_USERNAME}/${IMAGE_NAME}:${newTag}'

            # Publication de l'image sur le DockerHub
            echo "${DOCKER_PASSWORD}" | docker login --username '${DOCKER_USERNAME}' --password-stdin
            docker push '${DOCKER_USERNAME}/${IMAGE_NAME}:${newTag}'
            docker push '${DOCKER_USERNAME}/${IMAGE_NAME}:${originalTag}'
            docker logout
        """
    }
}
```

## Ressources

- [Documentation Jenkins - Configuration de l'agent](https://jenkins.io/doc/book/pipeline/syntax/#agent)
- [Documentation Jenkins - Utilisation de `credentials` dans les pipelines](https://jenkins.io/doc/pipeline/steps/credentials-binding/)