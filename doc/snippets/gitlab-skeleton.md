# Squelette Gitlab

Vous trouverez ci dessous un squelette de Jenkinsfile permettant d'intégrer un pipeline Jenkins avec une forge Gitlab.

Le fichier `Jenkinsfile` devrait être placé à la racine de votre projet pour qu'il soit pris en compte par Jenkins.

```groovy

pipeline {

    // Définition des déclencheurs pour votre pipeline
    // Dans notre cas, on indique que c'est Gitlab qui déclenchera le pipeline
    triggers {
        gitlab(
            // Ce pipeline devrait être déclenché sur un "Push"
            triggerOnPush: true,
            
            // Ce pipeline devrait être déclenché sur une ouverture de "Merge Request"           
            triggerOnMergeRequest: true,
            
            // Ce pipeline s'applique pour toutes les branches, quelles qu'elles soient.         
            branchFilterType: 'All',
            
            // Ne pas annuler les exécutions du pipeline en attente lors de nouvelles mises à jour d'état.         
            cancelPendingBuildsOnUpdate: false      
        )
    }

    // Définition des options de configuration pour votre pipeline
    options {
        // Ce pipeline devrait utiliser la connexion définie pour la communication avec le serveur Gitlab.
        // Voir la configuration du serveur Jenkins pour plus d'informations.
        gitLabConnection('Gitlab MIM')                    
                                                        
        // Liste des différents "stages" du pipeline. Cette liste doit correspondre aux labels des stages définis ci dessous.
        gitlabBuilds(builds: ['My first stage'])  
    }

    // Définition des différents stages du pipeline
    stages {

        // Définition de notre premier stage. Le label est arbitraire.
        // Un pipeline peut contenir autant de stages que nécessaire.
        stage('My first stage') {

            // Définition des étapes de notre stage
            steps {
                // On utilise une section "script" afin de capturer les potentielles
                // erreurs d'exécution et ainsi pouvoir notifier Gitlab de l'échec du stage.
                script {
                    try {
                        // Insérer ici vos commandes à exécuter.
                        // Le script est multi lignes.
                        // Si une commande retourne un code différent de 0, alors le stage s'interrompra.
                        sh '''
                            echo "Foo Bar"
                        '''

                        // Si l'exécution s'est correctement déroulée, on notifie Gitlab du succès du stage.
                        updateGitlabCommitStatus name: STAGE_NAME, state: 'success'

                    } catch (Exception e) {
                        // Si une erreur est survenue (i.e. un code retour différent de 0)
                        // alors on marque l'exécution du pipeline comme échoué.
                        // Il est possible d'utiliser 'UNSTABLE' pour ne pas interrompre
                        // l'exécution du pipeline (par exemple sur une erreur de "linting" ?).
                        currentBuild.result = 'FAILURE'

                        // On notifie Gitlab de l'échec d'exécution du stage
                        updateGitlabCommitStatus name: STAGE_NAME, state: 'failed'
                    }
                }
            }
        }

    }

}
```