# Utiliser un environnement Docker personnalisé

Il est possible d'exécuter les stages d'un pipeline à l'intérieur d'un environnement Docker personnalisé. Cela permet d'installer par exemple d'installer les dépendances nécessaires à l'exécution de certains tests sans avoir à "polluer" le contexte de l'exécuteur.

Chaque exécution du pipeline sera exécuté dans un environnement "propre". Le conteneur utilisé pour l'exécution sera automatiquement supprimé par Jenkins une fois celle ci terminée.

## Snippet

```groovy
// Jenkinsfile

pipeline {
    // On indique que l'on souhaite configurer l'agent d'exécution
    // pour notre pipeline.
    agent {

        // On spécifie que l'on souhaite utiliser un Dockerfile spécifique pour la construction
        // du conteneur d'exécution.
        dockerfile {
            
            // Le fichier Dockerfile s'appelle "Dockerfile" (pas particulièrement utile dans cet exemple)
            filename 'Dockerfile'

            // On indique que le contexte de construction de l'image du conteneur est le répertoire "ci" contenu dans notre projet. Il est ainsi possible de copier des fichiers dans notre conteneur depuis notre dépôt de code au moment de la construction de l'image.
            dir 'ci'
        }
    }
}

```

## Ressources

- [Documentation Jenkins - Définition de l'agent dans un pipeline déclaratif](https://jenkins.io/doc/book/pipeline/syntax/#agent)