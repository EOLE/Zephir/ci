# Création d'un nouveau pipeline

## Procédure

### Sur Gitlab (si votre projet n'est pas public)

1. Accéder à la page du projet sur Gitlab avec un compte ayant le rôle `Developer` (au minimum)
2. Naviguer sur la page `Settings` associée à votre profil utilisateur.
3. Naviguer sur l'onglet `Access Tokens`
4. Dans la section `Personal Access Tokens`, créer un nouveau jeton d'accès avec les droits suivants:
    - `read_repository`
    - `api`
5. Noter le `Personal Access Token` généré. Il permettra d'authentifier le pipeline pour lui permettre l'accès au dépôt de code.

### Sur Jenkins

1. Se connecter sur Jenkins
2. Cliquer sur `Nouveau item`.
3. Définir un nom pour votre nouveau pipeline puis sélectionner `Pipeline`. Cliquer sur `OK`
4. Dans la section `Build Triggers`, sélectionner `Build when a change is pushed to GitLab. GitLab webhook URL [...]`. Noter l'URL indiquée, elle permettra de configurer l'intégration côté Gitlab.
5. Dans la section `Enabled GitLab triggers`, sélectionner/configurer les champs suivants:
    - `Push Events`
    - `Opened Merge Request Events`
    - `Rebuild open Merge Requests` -> `On push to source or target branch`
    - `Comments`
6. Dans la section `Pipeline`:
    1. Définir le champ `Definition` à `Pipeline script from SCM`
    2. Définir le champ `SCM` à `Git`
    3. Dans la section `Repositories`:
        1. Définir le champ `Repository URL` à l'URL HTTP du dépôt Git de votre projet Gitlab
        2. Si votre projet est privé, définir le champs `Credentials` en créant une nouvelle paire identifiant/mot de passe à partir des jetons d'identification précedemment créés sur Gitlab.
    4. Dans la section `Branches to build`:
        1. Définir le champ `Branch Specifier (blank for 'any')` à `origin/${gitlabSourceBranch}`

### Sur Gitlab

1. Accéder à la page du projet sur Gitlab avec un compte ayant le rôle `Maintainer` (au minimum)
2. Cliquer sur `Settings` puis `Integrations`
3. Dans la section `Integrations`, renseigner le champ `URL` avec l'URL notée précedemment sur Jenkins.
4. Dans la section `Trigger`, sélectionner:
    - `Push events`
    - `Tag push events`
    - `Comments`
    - `Merge request events`
