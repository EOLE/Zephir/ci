# Intégration Gitlab / Jenkins

La procédure suivante décrit la mise en place d'une instance de Jenkins ainsi
que son intégration avec une forge Gitlab dans le contexte EOLE.

> **/!\ Attention** La procédure suivante ne permet pas d'obtenir un
> configuration sécurisée pour une instance publique de Jenkins.

## Procédure

### A. Créer une instance de Jenkins sur l'Open Nebula

1. Se connecter sur [l'OpenNebula EOLE](http://one.eole.lan/).
2. Instancier une nouvelle machine virtuelle [`jenkins-master`](http://one.eole.lan/#templates-tab/55793). L'image disque utilisée provient du projet [Cadoles de génération d'images disque](https://forge.cadoles.com/Cadoles/cadoles-vm).
3. Se connecter sur http://<ip_machine>:8080/ avec les identifiants `admin/cadoles`.

### B. Installer les plugins Jenkins pour l'intégration avec OpenNebula

1. Sur la page d'accueil de Jenkins, cliquer sur le lien `Administrer Jenkins` puis `Gestion des plugins`
2. Sélectionner l'onglet `Disponibles`, puis dans le champs de recherche, filtrer pour afficher les plugins:
    - [Gitlab authentication plugin](http://wiki.jenkins-ci.org/display/JENKINS/Gitlab+OAuth+Plugin)
    - [Gitlab Plugin](https://wiki.jenkins.io/display/JENKINS/GitLab+Plugin)
3. Sélectionner les 2 plugins puis cliquer sur le bouton `Télécharger maintenant et installer après redémarrage`.
4. Attendre la fin de l'installation et le redémarrage de Jenkins.

### C. Configurer Gitlab pour l'interaction avec Jenkins

1. Sur la forge [Gitlab](https://gitlab.mim.ovh), avec un compte administrateur, accéder à l'`espace d'administration`.
2. Sélectionner l'onglet `Applications` puis créer une nouvelle application pour Jenkins. Dans le champ `Redirect URL`, renseigner `http://<ip_machine>:8080/securityRealm/finishLogin`. Noter les valeurs `Client Secret` et `Client ID`.
3. Accéder à la page `Profile` du compter utilisateur. Sélectionner l'onglet `Access Tokens`.
4. Créer un nouveau `Personal Token`. Dans la section `Scopes`, sélectionner `api` et `read_repository`. Noter la valeur de l'`Access Token`

### D. Configurer Jenkins pour l'interaction avec Gitlab

1. Sur la page d'accueil de Jenkins, cliquer sur `Administrer Jenkins` puis `Configurer la sécurité globale`.
2. Dans la section `Contrôle de l'accès`:
    1. Sélectionner `Gitlab Authentication Plugin`
    2. Renseigner les champs suivants dans la section apparue `Global Gitlab OAuth Settings`:
        - GitLab Web URI - URL de votre instance Gitlab
        - GitLab API URI - URL de votre instance Gitlab
        - Client ID - Le `Client ID` noté précedemment
        - Client Secret - Le `Client Secret` noté précedemment
    3. Dans la section `Autorisations`, sélectionner `Les utilisateurs connectés peuvent tout faire`.
3. Retourner sur la page d'accueil, cliquer sur `Administrer Jenkins` puis `Configurer le système`.
4. Dans la section `Gitlab`, renseigner les champs `Connection name` et `Gitlab host URL`.
5. Dans le champ `Credentials`, cliquer sur le bouton `Ajouter` puis créer un nouveau `Credentials` de type `Gitlab API token`. Utiliser la valeur du `Access Token` noté précedemment.
6. Tester la connexion en cliquant le bouton `Test Connection`
7. Enregistrer la configuration en cliquant sur le bouton `Enregistrer`.

## Ressources

- [Cadoles - Recettes de génération d'image disque pour machines virtuelles](https://forge.cadoles.com/Cadoles/cadoles-vm)
- [Jenkins - Gitlab plugin](https://wiki.jenkins.io/display/JENKINS/GitLab+Plugin)
- [Jenkins - Gitlab authentication plugin](http://wiki.jenkins-ci.org/display/JENKINS/Gitlab+OAuth+Plugin)
- [Jenkins - Gitlab Plugin - Configuration générale](https://github.com/jenkinsci/gitlab-plugin#global-plugin-configuration)
- [Blogpost - Intégrer Jenkins à Gitlab](https://medium.com/@teeks99/continuous-integration-with-jenkins-and-gitlab-fa770c62e88a)



