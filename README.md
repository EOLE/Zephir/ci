# Zéphir CI

Projet d'agrégation des ressources concernant le pipeline d'intégration continue de Zéphir.

## Documentation

### Installation et configuration

- [Intégration Jenkins/Gitlab](./doc/gitlab-jenkins.md)
- [Création d'un exécuteur pour Jenkins](./doc/slave-creation.md)
- [Création d'un nouveau pipeline dans Jenkins](./doc/pipeline-creation.md)

### Snippets

- [Squelette minimal pour l'intégration avec Gitlab](./doc/snippets/gitlab-skeleton.md)
- [Utiliser un environnement Docker personnalisé pour son pipeline](./doc/snippets/use-custom-docker-env.md)
- [Construire une image Docker à partir des sources](./doc/snippets/create-docker-image.md)
- [Envoyer un courriel au développeur](./doc/snippets/send-email.md)
